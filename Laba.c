#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>              
#include <math.h>
#include <stdbool.h>
#include <float.h>
#include <malloc.h>

void processor();
float calculator(int function, float a, float x);
float getUserInput(char* title);
float calcG(float a, float x);
float calcF(float a, float x);
float calcY(float a, float x);
void clearStdin();

/// <summary>
/// The main method of the program
/// </summary>
/// <returns>Result</returns>
int main()
{
    while (true)
    {
        processor();
        printf("Input 'y' for continue or 'n' for exit: \n");
        char con;
        scanf_s("%c", &con, 1);
        clearStdin();
        if (con == "n") 
        {
            break;
        }
    }
    return 0;
}

/// <summary>
/// The main method of the program
/// </summary>
void processor()
{
    float f = getUserInput("Select formula:\n 1 - G\n 2 - F\n 3 - Y\n ");
    float a = getUserInput("Enter a: \n");
    float x1 = getUserInput("Enter x1: \n");
    float x2 = getUserInput("Enter x2: \n");
    float dx = getUserInput("Enter dx: \n");
    printf("            a           |          x            |  f(x)   \n");
    printf("  -------------------------------------------------------- \n");
    float x = x1;
    float minResult = FLT_MAX;
    float maxResult = FLT_MIN;
    float* results = NULL; int i = 0;
    while (x <= x2)
    {
        results = (float*)realloc(results, (i + 1) * sizeof(float));
        results[i] = calculator(f, a, x);
        if (results[i] < minResult) 
        {
            minResult = results[i];
        }
        if (results[i] > maxResult) 
        {
            maxResult = results[i];
        }
        printf("       %f         |         %f      |   %f \n", a, x, results[i]);
        x += dx;
        if (x > x2) {
            break;
        }
        i++;
    }
    printf("\n\n");
    printf("Min value           |   Max value \n");
    printf("------------------------------------------ \n");
    printf("%f            |     %f \n", minResult, maxResult);
    printf("\n\n");
    free(results);
}

/// <summary>
/// Formula selection function
/// </summary>
/// <param name="function">Number of the selected function</param>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calculator(int function, float a, float x) 
{
    switch (function)
    {
    case 1:
        return calcG(a, x);  
        break;
    case 2:
        return calcF(a, x);
        break;
    case 3:
        return calcY(a, x);
        break;
    default: break;
    }
}

/// <summary>
/// Function for calculating the formula G
/// </summary>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calcG(float a, float x)
{
    return (10 * (-45 * pow(a, 2) + 49 * a * x + 6 * pow(x, 2))) / (15 * pow(a, 2) + 49 * a * x + 24 * pow(x, 2));
}

/// <summary>
/// Function for calculating the formula F
/// </summary>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calcF(float a, float x)
{
    return tan((5 * pow(a, 2) + 34 * a * x + 45 * pow(x, 2)) * M_PI / 180);
}

/// <summary>
/// Function for calculating the formula Y
/// </summary>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calcY(float a, float x)
{
    return -1 * asin((7 * pow(a, 2) - a * x - 8 * pow(x, 2)) * M_PI / 180);
}

/// <summary>
/// Getting user Input
/// </summary>
/// <param name="title">Offer text</param>
/// <returns>Entered value</returns>
float getUserInput(char* title)
{
    printf(title);
    float p;
    if (!scanf_s("%f", &p))
    {
        printf("Input error!");
        return 0;
    }
    else
        return p;
}

/// <summary>
/// Clear Stdin
/// </summary>
void clearStdin()
{
    while (getchar() != '\n');
}